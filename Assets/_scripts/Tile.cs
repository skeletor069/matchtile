﻿using UnityEngine;
using System.Collections;

public class Tile {

	public int rowNumber;
	public int columnNumber;

	float tileValue = 0;

	public Tile(int rowNumber, int columnNumber){
		this.rowNumber = rowNumber;
		this.columnNumber = columnNumber;
	}

	public void UpdateRowColumn(int rowNumber, int columnNumber){
		this.rowNumber = rowNumber;
		this.columnNumber = columnNumber;
	}

	public void SetTileValue(float tileValue){
		this.tileValue = tileValue;
	}

	public float GetTileValue(){
		return tileValue;
	}
}

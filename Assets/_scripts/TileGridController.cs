﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TileGridController : MonoBehaviour {

	Tile[,] tileGrid;
	int ROW_COUNT = 7;
	int COLUMN_COUNT = 7;

	List<TileUI> gridUIList;
	TileUI[,] gridUIMatrix;
	public Transform gridUIHolderPanel;

	public static List<TileUI> matchTileList = new List<TileUI> ();

	void Awake () {
		Physics2D.gravity = new Vector3 (0, -100, 0);
		gridUIMatrix = new TileUI[ROW_COUNT, COLUMN_COUNT];
		tileGrid = new Tile[ROW_COUNT, COLUMN_COUNT];
		for (int i = 0; i < ROW_COUNT; i++) {
			for(int j = 0 ; j < COLUMN_COUNT; j++){
				int tileValue = Random.Range (0,6);
				tileGrid[i,j] = new Tile (i,j);
				tileGrid [i, j].SetTileValue (tileValue);
			}
		}


	}
	
	// Update is called once per frame
	void Start () {
		//gridUIList = new List<TileUI> ();
		int childCount = gridUIHolderPanel.childCount;
		for (int i = 0; i < childCount; i++) {
			TileUI temp = gridUIHolderPanel.GetChild (i).GetComponent<TileUI> ();
			temp.SetTileData (tileGrid [i / COLUMN_COUNT, i % COLUMN_COUNT]);
			temp.SetController (this);
			gridUIMatrix [i / COLUMN_COUNT, i % COLUMN_COUNT] = temp;
			//gridUIList.Add (temp);
		}
	}

	public void TileClicked(TileUI tileUI){
		matchTileList.Clear ();
		CheckAdjescent (tileUI);
		Debug.LogError (matchTileList.Count);
		if (matchTileList.Count > 1) {
			for (int i = 0; i < matchTileList.Count; i++) {
				matchTileList [i].toBeDestroyed = true;
				matchTileList [i].gameObject.SetActive (false);
			}

			UpdateGridPositions ();
		}
	}

	void UpdateGridPositions(){
		for (int column = 0; column < COLUMN_COUNT; column++) {
			List<TileUI> acList = GetActiveTilesInColumn (column);
			List<TileUI> deacList = GetDeactiveTilesInColumn (column);
			int activeCount = acList.Count;
			for (int i = 0; i < activeCount; i++) {
				acList [i].GetTileData ().UpdateRowColumn (i, column);
				gridUIMatrix [i, column] = acList [i];
			}

			for (int i = 0; i < ROW_COUNT - activeCount; i++) {
				Tile tileData = deacList [i].GetTileData ();
				tileData.UpdateRowColumn (activeCount + i, column);
				tileData.SetTileValue (Random.Range(0,6));

				deacList [i].toBeDestroyed = false;
				Vector2 anchoredPos = deacList [i].GetAnchoredPosition ();
				anchoredPos.y = 150 + i*110;
				deacList [i].SetAnchoredPosition (anchoredPos);
				deacList [i].gameObject.SetActive (true);
				deacList [i].SetTileData (tileData);
				gridUIMatrix [activeCount + i, column] = deacList [i];
			}
		}
	}
	List<TileUI> activeTiles = new List<TileUI> ();
	List<TileUI> deactiveTiles = new List<TileUI> ();
	List<TileUI> GetActiveTilesInColumn(int columnNumber){
		activeTiles.Clear ();
		for (int i = 0; i < ROW_COUNT; i++) {
			if (!gridUIMatrix [i, columnNumber].toBeDestroyed)
				activeTiles.Add (gridUIMatrix[i,columnNumber]);
		}
		return activeTiles;
	}
	List<TileUI> GetDeactiveTilesInColumn(int columnNumber){
		deactiveTiles.Clear ();
		for (int i = 0; i < ROW_COUNT; i++) {
			if (gridUIMatrix [i, columnNumber].toBeDestroyed)
				deactiveTiles.Add (gridUIMatrix[i,columnNumber]);
		}
		return deactiveTiles;
	}

	void CheckAdjescent(TileUI tileUI){
		if (!matchTileList.Contains (tileUI)) {
			matchTileList.Add (tileUI);

			if (LeftMatchFound (tileUI)) {
				CheckAdjescent (gridUIMatrix [tileUI.GetTileData ().rowNumber, tileUI.GetTileData ().columnNumber - 1]);
			}

			if (TopMatchFound (tileUI)) {
				CheckAdjescent (gridUIMatrix [tileUI.GetTileData ().rowNumber + 1, tileUI.GetTileData ().columnNumber]);
			}

			if (RightMatchFound (tileUI)) {
				CheckAdjescent (gridUIMatrix [tileUI.GetTileData ().rowNumber, tileUI.GetTileData ().columnNumber + 1]);
			}

			if (BottomMatchFound (tileUI)) {
				CheckAdjescent (gridUIMatrix [tileUI.GetTileData ().rowNumber - 1, tileUI.GetTileData ().columnNumber]);
			}
		}
	}

	bool LeftMatchFound (TileUI tileUI)
	{
		return tileUI.GetTileData ().columnNumber > 0 && gridUIMatrix [tileUI.GetTileData ().rowNumber, tileUI.GetTileData ().columnNumber - 1].GetTileData ().GetTileValue () == tileUI.GetTileData ().GetTileValue ();
	}

	bool TopMatchFound (TileUI tileUI)
	{
		return tileUI.GetTileData ().rowNumber < ROW_COUNT-1 && gridUIMatrix [tileUI.GetTileData ().rowNumber + 1, tileUI.GetTileData ().columnNumber].GetTileData ().GetTileValue () == tileUI.GetTileData ().GetTileValue ();
	}

	bool RightMatchFound (TileUI tileUI)
	{
		return tileUI.GetTileData ().columnNumber < COLUMN_COUNT - 1 && gridUIMatrix [tileUI.GetTileData ().rowNumber, tileUI.GetTileData ().columnNumber + 1].GetTileData ().GetTileValue () == tileUI.GetTileData ().GetTileValue ();
	}

	bool BottomMatchFound (TileUI tileUI)
	{
		return tileUI.GetTileData ().rowNumber > 0 && gridUIMatrix [tileUI.GetTileData ().rowNumber - 1, tileUI.GetTileData ().columnNumber].GetTileData ().GetTileValue () == tileUI.GetTileData ().GetTileValue ();
	}
}

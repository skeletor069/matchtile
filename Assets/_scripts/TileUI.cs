﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TileUI : MonoBehaviour {

	Animator anim;
	Button btn;
	Tile tileData;
	RectTransform rectTransform;
	TileGridController controller;
	public bool toBeDestroyed = false;
	int animColorIndex = Animator.StringToHash("colorIndex");

	void Awake () {
		anim = GetComponent<Animator> ();
		btn = GetComponent<Button> ();
		btn.onClick.AddListener (delegate{
			Clicked();
		});
		rectTransform =  GetComponent<RectTransform>();
	}

	public void SetController(TileGridController controller){
		this.controller = controller;
	}

	public void SetTileData(Tile tileData){
		this.tileData = tileData;
		anim.SetFloat (animColorIndex, tileData.GetTileValue ());
	}

	public Tile GetTileData(){
		return tileData;
	}

	public Vector2 GetAnchoredPosition(){
		return rectTransform.anchoredPosition;
	}

	public void SetAnchoredPosition(Vector2 anchoredPos){
		rectTransform.anchoredPosition = anchoredPos;
	}

	void Clicked()
	{
		controller.TileClicked (this);
	}

	public void AddToMatchListWithNeighbour(){
	
	}
}
